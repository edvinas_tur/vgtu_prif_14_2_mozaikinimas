﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
namespace Mozaikinimo_WebService.Tests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void emailValidationTest()
        {
            email_validator ev = new email_validator();
            Assert.AreEqual(true, ev.IsValidEmail("meeh@gmail.com"));

        }

        [TestMethod]
        public void mosaicRequestValidationTest()
        {
            CreateMosaicRequestValidator crv = new CreateMosaicRequestValidator("lalala", "C:\\Users\\Darius\\Desktop\\web api\\vgtu_prif_14_2_mozaikinimas\\vgtu_prif_14_2_mozaikinimas\\Mozaikinimo_WebService\\obj\\Debug\\Mozaikinimo_WebService.csproj.FileListAbsolute.txt");
            crv.validate();
            Assert.IsTrue(crv.isValid());
        }
        [TestMethod]
        public void regValidationTest()
        {
            CreateRegValidation rv = new CreateRegValidation("something@gmail.com", "watt", "password1");
            rv.validate();
            Assert.IsTrue(rv.isValid());

        }

        [TestMethod]
        public void errorMessageTest()
        {
            CreateRegValidation crv = new CreateRegValidation("asda", "", "asd");
            crv.validate();
            Assert.IsNotNull(crv.getErrorMessage());
            Assert.IsTrue(crv.getErrorMessage().Contains("password"));
            Assert.IsTrue(crv.getErrorMessage().Contains("email"));
            Assert.IsTrue(crv.getErrorMessage().Contains("first name"));
        }

        [TestMethod]
        public void urlValidatorTest()
        {
            
            String url = "url";
            URLValidtor uv = new URLValidtor(url, 2);
            uv.validate();
            Assert.IsFalse(uv.isValid());
            Assert.IsTrue(uv.getErrorMessage().Contains(url));
        }

        [TestMethod]
        public void imageExtensioValdatorTest()
        {
            String url = "C:\\Users\\Darius\\Downloads\\P0003\\P0003\\IR0036EY.Jpeeeg";
            ImageExtensionValidator iet = new ImageExtensionValidator(url, 20);
            iet.validate();
            Assert.IsFalse(iet.isValid());
            Assert.IsTrue(iet.getErrorMessage().Contains("url extension"));
        }

        [TestMethod]
        public void resourceAvailableHTTPValidatorTest()
        {
            ResourceAvailableThroughHTTPValidator rathvt = new ResourceAvailableThroughHTTPValidator("qwerty",0);
            rathvt.validate();
            Assert.IsFalse(rathvt.isValid());
            Assert.IsNotNull(rathvt.getErrorMessage());
            Assert.IsTrue(rathvt.getErrorMessage().Contains("cannot be reached over the HTTP"));
        }
        [TestMethod]
        public void abstractImageValidatorTest()
        {
            var mock = new Mock<AbstractImageValidator>() { CallBase = true };
            //mockMyViewModel.Setup(x => x.isValid()).Returns(false);
            //mock.CallBase = true;

            //var obj = mock.Object;

            mock
               .Setup(c => c.isValid())
               .Equals(false);

            //mockas.CallBase = true;
            //mockas.Setup(t => t.validate());

            // act
            //mockMyViewModel.Object.isValid();
            //Assert.
            //AbstractImageValidator test = mockas.Object;
            //test.validate();
            //Assert.AreEqual(true, mockas.CallBase);
            // assert
            // mockas.Verify(t => t.validate());
            // MockRepository.v
        }

    }
}
