﻿using System;
using System.Net;

/// <summary>
/// Summary description for ImagesURLFileValidtor
/// </summary>
public class CreateMosaicRequestValidator : Validator
{

    private String file;
    private bool valid = false;
    private String errorMessage = "";
    private String algorythm;
    public CreateMosaicRequestValidator(String _algorythm, String _file)
	{

        file = _file;
        algorythm =_algorythm;

	}

    public void validate()
    {

        String ext = System.IO.Path.GetExtension(file);

        if (algorythm == null || algorythm == ""){

            valid = false;
            errorMessage += "alogrythm attribute in SOAP request is empty or null.  ";
        
        }

        if (file == null)
        {
            valid = false;
            errorMessage += "file attribute in SOAP request is empty or null.  ";
        }
        else if (!doFileExists(file))
        {
            valid = false;
            errorMessage += "File cannot be reached over the HTTP or downloaded localy, check your url and try again. ";
        }


        if (ext == null) {
            valid = false;
            errorMessage += "docName attribute in SOAP request is empty or null.  ";
        }
        else if (ext.ToLower() == ".txt")
        {
            valid = true;
        }
        else {
            valid = false;
            errorMessage += "Imeges urls file's extension must be .txt  ";
        }
             
    }

    public bool isValid()
    {
        return valid;
    }

    public string getErrorMessage()
    {
        return errorMessage;
    }

    private bool doFileExists( String urlString)
    {
        Boolean valid = false;
        HttpWebResponse response = null;
        try
        {
            var request = (HttpWebRequest)WebRequest.Create(urlString);
            request.Method = "HEAD";
            response = (HttpWebResponse)request.GetResponse();
            valid = true;
        }
        catch (Exception ex)
        {
            if (System.IO.File.Exists(urlString))     valid = true;
            else   valid = false;
        }
        finally
        {
            if (response != null) response.Close();
        }

        return valid;
    }
}