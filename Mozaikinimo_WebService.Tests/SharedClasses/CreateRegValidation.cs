﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CreateRegValidation
/// </summary>
public class CreateRegValidation : Validator
{

    private String email;
    private bool valid = true;
    private String errorMessage = "";
    private String fname;
    private String password;
    public CreateRegValidation(String _email, String _fname, String _password)
    {
        email = _email;
        fname = _fname;
        password = _password;
    }

    public string getErrorMessage()
    {
        return errorMessage;
    }

    public bool isValid()
    {
        return valid;
    }


    public void validate()
    {
        email_validator util = new email_validator();
        bool containsInt = password.Any(char.IsDigit);
        if (email == null || email == "")
        {

            valid = false;
            errorMessage += "email attribute in SOAP request is empty or null.  ";

        }

        if (fname == null || fname == "")
        {
            valid = false;
            errorMessage += "first name attribute in SOAP request is empty or null.  ";
        }

        if (password == null || password == "")
        {
            valid = false;
            errorMessage += "password attribute in SOAP request is empty or null.  ";
        }

        if (password.Length < 4 && password.Length>12)
        {
            valid = false;
            errorMessage += "password length should contain 5-12 symbols.   ";
        }
        if (fname.Length < 3 && fname.Length > 12)
        {
           valid = false;
            errorMessage += "first name should contain 3-12 symbols.   ";
        }
        if(containsInt == false)
        {
            valid = false;
            errorMessage += "password should contain atleast 1 digit.   ";
        }
        if (util.IsValidEmail(email) == false)
        {
            valid = false;
            errorMessage += "wrong email address.  ";
        }
    

    }
}