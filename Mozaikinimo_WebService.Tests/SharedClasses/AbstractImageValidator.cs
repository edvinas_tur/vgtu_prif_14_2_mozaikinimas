﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AbstractValidator
/// </summary>
public abstract class AbstractImageValidator : Validator
{

    protected String urlString;
    protected bool valid = false;
    protected String errorMessage = "";
    protected int lineNumber;

    protected AbstractImageValidator(String _urlString, int _lineNumber)
	{
        lineNumber = _lineNumber;
        urlString = _urlString;	
	}

    public abstract void validate();

    public bool isValid()
    {
        return valid;
    }

    public string getErrorMessage()
    {
        return errorMessage;
    }
}