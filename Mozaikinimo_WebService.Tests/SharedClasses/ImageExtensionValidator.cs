﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ImageExtensionValidator
/// </summary>
public class ImageExtensionValidator : AbstractImageValidator
{

    public ImageExtensionValidator(String _urlString, int _lineNumber): base ( _urlString,  _lineNumber)
    { }

    public override void validate()
    {

        try
        {
            String ext = System.IO.Path.GetExtension(urlString);

            if (ext == null)
            {

                valid = false;
                errorMessage = "Image url's file contains invalid url  on line " + lineNumber;

            }
            else if (ext.ToLower() == ".jpeg" || ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
            {

                valid = true;
            }
            else
            {

                valid = false;
                errorMessage = "Image url's file contains invalid url extension (" + ext + ") on line " + lineNumber + ". Possible extensions are: jpeg, png, jpg.";
            }

        }

        catch (Exception ex) {
            valid = false;
            errorMessage = "Image url's file contains invalid url extension  on line " + lineNumber + ". Possible extensions are: jpeg, png, jpg.";  
        }

    }
}