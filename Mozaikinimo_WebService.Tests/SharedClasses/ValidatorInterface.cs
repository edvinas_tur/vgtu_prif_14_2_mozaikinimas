﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Interface for all validators in app
/// </summary>
public interface Validator
{
     void validate();
     bool isValid();
     String getErrorMessage();

}