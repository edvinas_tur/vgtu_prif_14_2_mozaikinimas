﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// Summary description for Class1
/// </summary>
public class ResourceAvailableThroughHTTPValidator : AbstractImageValidator
{
    public ResourceAvailableThroughHTTPValidator(String _urlString, int _lineNumber): base(_urlString, _lineNumber)
    { }
    public override void validate()
    {
      
        HttpWebResponse response = null;

        try
        {            
            var request = (HttpWebRequest)WebRequest.Create(urlString);
            request.Method = "HEAD";
            response = (HttpWebResponse)request.GetResponse();
            valid = true;
        }
        catch (Exception ex)
        {

            if (System.IO.File.Exists(urlString))
            {
                valid = true;

            }
            else {
                valid = false;
                errorMessage = "Image with url: " + urlString + " cannot be reached over the HTTP, check your url and try again, image urls file line: " + lineNumber;
            }
          
        }
        finally
        {
            if (response != null)
            {
                response.Close();
            }
        }
    }
}