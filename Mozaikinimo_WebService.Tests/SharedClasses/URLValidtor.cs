﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ImagesURLFileContentValidator
/// </summary>
public class URLValidtor : AbstractImageValidator
{
    public URLValidtor(String _urlString, int _lineNumber) : base(_urlString, _lineNumber) { }
    public override void validate()
    {
        try
        {
             valid = this.IsValidUri(urlString);
      
            if (!valid) errorMessage = "Errors with your image url's file content. String (" + urlString + ") on line " + lineNumber + " is not a valid URL string";
        }
        catch (Exception ex) {
            valid = false;
            errorMessage = "Errors with your image urls' file content. String  (" + urlString + ") on line " + lineNumber + " is not a valid URL string"; 
        }
    }
 

    private Boolean IsValidUri(String uri)
    {
        try
        {
            new Uri(uri);
            return true;
        }
        catch
        {
            return false;
        }
    }
}