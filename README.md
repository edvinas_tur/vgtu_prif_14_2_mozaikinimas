# README #

Repozitorija skirta VGTU modulio "Programų kūrimo procesas" laboratoriniam darbui "Akies dugno vazidų mozaikinimas"

### How do I get set up? ###

Norint, kad proejktas veiktų reikalingas .NET Framework v4.5, Microsoft Visual Studio (2013 m versija ir vėliau), Git klientas ir TortoiseGit sąsaja darbui su VCS

1. Nueinam į repoziorijos tinklapį ir nukopijuojam HTTPS clone nuorodą į projektą : "https://edvinas_tur@bitbucket.org/edvinas_tur/vgtu_prif_14_2_mozaikinimas.git"
2. Sukuriame aplanką savo kompiuteryje kuriame talpinsime failus
3. Su tortoise git klientu (iškviečiam windows kontekstinį meniu savo sukurtame aplankale -> git clone) klonuojame projektą iš nuorodos pateiktos 2 žingsnyje
4. Vėl su tortoise git pakeičiame branch'ą į "development" (SVARBU! ) tortoise git -> switch/ checkout -> orgins -> development
5. Atidarome MS Visual Studio ir pasirenkame: File -> open -> Web Site -> (pasirenkame lokaliai nuklonuotą aplankalą "Mozaikinimas")
6. Norint paleisti Web Servisą ties failu Mozaikinimas_WS.asmx dešiniu myguku paspaudžime View in Browser
7. Sekmės! :)

### Contribution guidelines ###
Visi pekeitimai daromi TIK ant development brancho
Visa dokumentacija yra saugoma aplankuose ant "documentation" brancho


