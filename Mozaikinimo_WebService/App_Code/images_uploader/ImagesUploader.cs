﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Class which is responsible for images uploading and validation in WEB service
/// </summary>
public  class ImagesUploader
{
    public String exx;
    private List <AbstractImageValidator> validators = new List <AbstractImageValidator>();
    private List <String> imagesSaveDirectories = null;
    private String storageFolderPath = null;
    private String storageFolderRelativePath = null;
    private String timeStamp = null;

    /// <summary>
    /// This method uploads images from file with images storage urls in each line.
    /// Images are stored in Storage folder.
    /// </summary>
    /// <param name="storagePath">path of the storage</param>
    /// <param name="userId">current authenticated users ID</param>
    /// <returns>true if success or false otherwise</returns>
    public  bool uploadImagesFromFile(String storagePath, int userId)
	{
        imagesSaveDirectories = new List<String>();
        timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");

        try
        {
            System.IO.DirectoryInfo directory = Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~\\Storage\\")+ userId+"\\"+ timeStamp);
            System.IO.StreamReader file = new System.IO.StreamReader(storagePath);
            
            storageFolderPath = System.Web.Hosting.HostingEnvironment.MapPath("~\\Storage\\") + userId +"\\"+ timeStamp + "\\";
            storageFolderRelativePath  = "~\\Storage\\" +userId + "\\" + timeStamp + "\\";
             String singleURL;
 

            int lineNo = 0;

            while (!file.EndOfStream)
            {
                validators.Clear();   
                singleURL = file.ReadLine();
                if (singleURL == "") continue; // If empty string in file ignore

                validators.Add(new URLValidtor(singleURL, lineNo));
                validators.Add(new ImageExtensionValidator(singleURL, lineNo));
                validators.Add(new ResourceAvailableThroughHTTPValidator(singleURL, lineNo)); //TODO uncomment this on production enviroment


                foreach (AbstractImageValidator validator in validators)
                {
                    validator.validate();

                    if (!validator.isValid())
                    {
                        return false; // exiting function in case any validation is invalid
                    }                   
                }

                // downloading after validation

                System.Net.WebClient webClient = new System.Net.WebClient();
                Uri uri = new Uri(singleURL);
                string fileName = uri.Segments.Last();
                webClient.DownloadFile(singleURL, storageFolderPath + fileName);
                imagesSaveDirectories.Add(storageFolderRelativePath + fileName);
                //
                lineNo++;
 
            }

            file.Close();
           
        }
        catch (Exception ex) {
           
            Log.Write(ex.Message + ex.StackTrace);
            return false;
        }

        return true;
	}

    /// <summary>
    /// This method returns time when mosaicing invoked
    /// </summary>
    /// <returns>timestamp in whic all uploading procedure started</returns>
    public String getTimestamp() {
        return timeStamp;
    }

    /// <summary>
    /// This method returns all validators required for validation. Each validator can be either valid or with error message.
    /// </summary>
    /// <returns>Different typ of validators list associated with images upload from file storage paths</returns>
    public List<AbstractImageValidator> getValidators()
    {
        return validators;
    }

    /// <summary>
    /// This method returns directories where images are saved.
    /// </summary>
    /// <returns>imagesSaveDirectories</returns>
    public List<String> getImagesStoragePaths()
    {
        return imagesSaveDirectories;
    }

    /// <summary>
    /// This method returns storage path in which all users uploads are being saved.
    /// </summary>
    /// <returns>Returns storage folder path</returns>
    public String getStoragePath() {

        return storageFolderRelativePath;
    }
}