﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for showing all mosaicand uploads results for user.
/// </summary>
    public class Results
    {
    /// <summary>
    /// The list of all errors if any errors
    /// </summary>
    private List<String> errors = new List<String>();

    /// <summary>
    /// Current authenticated userID
    /// </summary>
    private int userId;

    /// <summary>
    /// Number od last N days result will be shown
    /// </summary>
    private int days = -1;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="userId">current authenitcated users Id</param>
        public Results(int userId) {
            this.userId = userId;
        }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="userId">current authenitcated users Id</param>
    /// <param name="pastDays">Then N last days of results will be showed</param>
    public Results(int userId, int pastDays)
        {
            this.userId = userId;
            this.days = pastDays;
        }

    /// <summary>
    /// This method returns errors
    /// </summary>
    /// <returns>Errors associated with results showing procedure</returns>
        public List<String> getErrors() {

            return this.errors;
        }

    /// <summary>
    /// This method raturns results XML object in which all the data are grouped by timestamps.
    /// </summary>
    /// <returns>Returns all the results grouped by timestamps</returns>
    public List<Timestamp> getResultsData()
        {

            List<Timestamp> timestamps = new List<Timestamp>();

            List<String> times = getTimestamps(userId, days);

            foreach (String singleTimestamp in times)
            {
            Timestamp timestamp = getSingleTimestampsData(singleTimestamp, userId);
                timestamps.Add(timestamp);
            }

            return timestamps;
        }

    /// <summary>
    /// This method gets all timestamps from database by User ID
    /// </summary>
    /// <param name="userId">Authenticated user ID</param>
    /// <param name="pastDays">Tinestamps only of N last days are returned</param>
    /// <returns>Distinct timestamps in given period and asociated with certain user</returns>
    private List<String> getTimestamps(int userId, int pastDays)
        {
            List<String> times = new List<String>();

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString))
            {
                connection.Open();

                string sqlCommand;

          
               sqlCommand = pastDays < 0 ? "SELECT DISTINCT timestamp FROM [uploads] WHERE userId = @userId ORDER BY timestamp DESC" : "SELECT DISTINCT timestamp FROM [uploads] WHERE userId = @userId AND created > @created ORDER BY timestamp DESC";

                using (var command = new SqlCommand(sqlCommand, connection))
                {
                    try
                    {
                        command.Parameters.Add("@userId", SqlDbType.Int).Value = userId;

                        if (pastDays > 0) {
                            
                            DateTime dateForButton = DateTime.Now.AddDays(-pastDays);
                           command.Parameters.Add("@created", SqlDbType.DateTime).Value = dateForButton;
                        }

                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                times.Add(dataReader["timestamp"].ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Write(ex.Message + ex.StackTrace);
                        this.errors.Add(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }

                }

            }
            return times;
        }

    /// <summary>
    /// This method build single timestamp results with all images uploads in original size and thumbnails. All mosaics and mosaic thumbnails
    /// </summary>
    /// <param name="timestamp">Time of results</param>
    /// <param name="userId">Current authenticated user Id</param>
    /// <returns>timestampDTO</returns>
    private Timestamp getSingleTimestampsData(String timestamp, int userId)
        {

        Timestamp timestampDTO = new Timestamp();
        timestampDTO.timestamp = timestamp;
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString))
            {
                connection.Open();

                string sqlCommand = "SELECT storagePath, thumbnail, mosaic  FROM [uploads] WHERE userId = @userId AND  timestamp = @timestamp";

                using (var command = new SqlCommand(sqlCommand, connection))
                {
                    command.Parameters.Add("@userId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@timestamp", SqlDbType.VarChar).Value = timestamp;

                    try
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                             while (dataReader.Read())
                            {
                                if (Boolean.Parse(dataReader["thumbnail"].ToString()) && !Boolean.Parse(dataReader["mosaic"].ToString()))
                                {
                                    timestampDTO.thumbnails.Add(getHttpUriFromPath(dataReader["storagePath"].ToString()));
                                }
                                else if (!Boolean.Parse(dataReader["mosaic"].ToString()) && !Boolean.Parse(dataReader["thumbnail"].ToString()))
                                {

                                    timestampDTO.images.Add(getHttpUriFromPath(dataReader["storagePath"].ToString()));
                                }
                                else if (!Boolean.Parse(dataReader["thumbnail"].ToString()) && Boolean.Parse(dataReader["mosaic"].ToString()))
                                {
                                    timestampDTO.mosaics.Add(getHttpUriFromPath(dataReader["storagePath"].ToString()));
                                }
                                else {
                                    timestampDTO.mosaicsThumbnails.Add(getHttpUriFromPath(dataReader["storagePath"].ToString()));
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Write(ex.Message + ex.StackTrace);
                        errors.Add(ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }
            return timestampDTO;
        }

    /// <summary>
    /// This method returns storage path in http format to access it via HTTP.
    /// </summary>
    /// <param name="stroragePath">path of the storage in relative format</param>
    /// <returns>output</returns>
    private string getHttpUriFromPath(String stroragePath) {
            String output = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Authority, stroragePath);
            output = output.Replace("~", "");
            output = output.Replace("\\", "/");
            return output;
        }   
    }



    
    
 