﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Linq;

/// <summary>
/// Web Service Main 
/// </summary>
[WebService(Namespace = "http://akies_dugno_vaizu_mozikinimas.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Xml.Serialization.XmlInclude(typeof(MosaicResponse)), System.Xml.Serialization.XmlInclude(typeof(SuccessfullRegistrationResponse)) , System.Xml.Serialization.XmlInclude(typeof(viewUploadedImagesResponse)), System.Xml.Serialization.XmlInclude(typeof(BadRequestResponse)), System.Xml.Serialization.XmlInclude(typeof(NotAuthorizedResponse)), System.Xml.Serialization.XmlInclude(typeof(BadImageUrlsFileResponse))]

// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Mozaikinimo_WS : System.Web.Services.WebService
{
    
    /// <summary>
    /// Constructor of Mozaikinimo_WS()
    /// </summary>
    public Mozaikinimo_WS()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /// <summary>
    /// This Web method creates mosaic of two eyes and returns HTTP urls where mosaics are stored.
    /// </summary>
    /// <param name="email">an email provided by user</param>
    /// <param name="password">a password provided by user</param>
    /// <param name="algorythm">selected algoryhtm</param>
    /// <param name="file">the file of the image</param>
    /// <returns>response for request, type changes dynamically depending on conditions etc.</returns>
    [WebMethod(Description = "This method creates two mosaic of eyes and returns two urls to stored mosaics on server")]
    public Response CreateMosaic(String email, String password, String algorythm, String file)
    {
        Authentication login = new Authentication(password, email);
       
        login.validate();
        // If user not authenticated returning auth errors
        if (!login.isValid()) {
            Response response = new NotAuthorizedResponse();
            response.build(login.getErrorMessage());
            return response;
        }
        // executing mosaic creation request
        BaseRequest mosaicReqest = new MosaicRequest(login.getUserId(), algorythm,file );
        mosaicReqest.execute();
        return mosaicReqest.getResponse();
    }

    /// <summary>
    /// This Web method is used as registration form on the Web service.
    /// </summary>
    /// <param name="email">an email provided by user</param>
    /// <param name="name">a name provided by user</param>
    /// <param name="password">a password provided by user</param>
    /// <returns>response for request, type changes dynamically depending on conditions etc.</returns>
    [WebMethod(Description = "This method is used as a registration form.")]
    public Response RegForm(string email, string name, string password)
    {
        BaseRequest registraionReq = new RegistrationRequest(email, name, password);
        registraionReq.execute();
        return registraionReq.getResponse();
    }

    /// <summary>
    /// This Web method is used to view all uploads of the user on the Web service.
    /// </summary>
    /// <param name="email">an email provided by user</param>
    /// <param name="password">a password provided by user</param>
    /// <param name="daysPeriod">the number of  N last days resurts wiil be shown</param>
    /// <returns>response for request, type changes dynamically depending on conditions etc.</returns>
    [WebMethod(Description = "This method is used to view all user's uploads on WEB service")]
    public Response viewUploadedIamges(string email, string password, int daysPeriod)
    {
        Response response = null;
        Authentication login = new Authentication(password, email);
        login.validate();


        if (!login.isValid())
        {
            response = new NotAuthorizedResponse();
            response.build(login.getErrorMessage());
            return response;
        }
        Results results = new Results(login.getUserId(), daysPeriod);
        List<Timestamp> responseData = results.getResultsData();

        if (results.getErrors().Count() > 0)
        {
            BadRequestResponse res = new BadRequestResponse();
            res.buildFromList(results.getErrors());
            response = (Response) res;
        }
        else
        {
            response = new viewUploadedImagesResponse();
            response.build(responseData);
        }
  
        return response;

    }
}

    
