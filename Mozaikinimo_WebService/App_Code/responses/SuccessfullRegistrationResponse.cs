﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

    /// <summary>
    /// This class is responsible for the response of successfull registration to user.
    /// </summary>
    public class SuccessfullRegistrationResponse : Response
    {
        /// <summary>
        /// This method builds a response with provided message for user
        /// </summary>
        /// <param name="_parameters">Message for successfully registarred user</param>
        public override void build(object _parameters)
        {
            this.setStatusCode(200);
            this.responseMessage = "Resgistration data stored successfully";
        }
    }
