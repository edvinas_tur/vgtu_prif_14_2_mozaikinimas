﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This is an abstract class for creating all responses.
/// </summary>
public abstract class Response
{

    /// <summary>
    /// Initialisation of Response status code
    /// </summary>
    public int statusCode = 200;

    /// <summary>
    /// Initialisation of response message
    /// </summary>
    public String responseMessage = "";

    /// <summary>
    /// Abstract method for building response from given parameters
    /// </summary>
    /// <param name="_parameters">parameters of the response</param>
    public abstract void build(Object _parameters);

    /// <summary>
    /// This sets response status code. F.e. 500, 401 ...
    /// </summary>
    /// <param name="_statusCode">provided status code</param>
    protected void setStatusCode(int _statusCode){
        this.statusCode = _statusCode;
        HttpContext.Current.Response.StatusCode = _statusCode;
    }

}