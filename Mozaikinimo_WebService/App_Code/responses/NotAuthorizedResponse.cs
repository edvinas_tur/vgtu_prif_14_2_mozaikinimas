﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for not authorized access to WS response.
/// </summary>
/// 
public class NotAuthorizedResponse : Response
{
    /// <summary>
    /// builds unauthrized access response
    /// </summary>
    /// <param name="_parameters">Message for unauthorized user</param>
    public override void build(Object _parameters)
    {
        this.responseMessage = (String)_parameters;
        this.setStatusCode(401);
    }

}