﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for bad image url response building for user.
/// </summary>
public class BadImageUrlsFileResponse : Response
{
    /// <summary>
    /// The list of errors.
    /// </summary>
    public List<String> errors = new List<String>();

    /// <summary>
    /// Default constructor BadImageUrlsFileResponse()
    /// </summary>
    public BadImageUrlsFileResponse()
	{

	}

    /// <summary>
    /// Method build Response for user in case images urls file provided by user is invalid
    /// </summary>
    /// <param name="_parameters">Validators from which respnse is being built</param>
    public override void build(Object _parameters)
    {
        List<AbstractImageValidator> validationErrors = (List<AbstractImageValidator>)_parameters;
        foreach (AbstractImageValidator validationError in validationErrors)
        {

            if (!validationError.isValid())
                errors.Add(validationError.getErrorMessage());
        }
        this.setStatusCode(400);
    }
}