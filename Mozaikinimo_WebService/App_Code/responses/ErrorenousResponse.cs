﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible from bad request (code 400) response. Class id used to inform user what he entered invalid request argments or in similiar situation
/// </summary>
public class BadRequestResponse : Response
{
    /// <summary>
    /// The list of errors.
    /// </summary>
    public List<String> errors = new List<String>();

    /// <summary>
    /// Builds esponse from list of errors
    /// </summary>
    /// <param name="errors">provided errors</param>
    public void buildFromList(List<String> errors)
    {
        this.errors = errors;
        this.setStatusCode(400);
    }

    /// <summary>
    /// builds response from single error
    /// </summary>
    /// <param name="_parameters">parameters of the response</param>
    public override void build(Object _parameters)
    {
        this.responseMessage = (String) _parameters;
        this.setStatusCode(400);
    }


}