﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for successfull mosaic generation response for user.
/// </summary>
public class MosaicResponse : Response
{

    public List<String> createdMosaics = new List<String>();

    /// <summary>
    /// Method is used to build successful mosaic response 
    /// </summary>
    /// <param name="_parameters">List of mosaics HTTP URI's on server</param>
    public override void build(Object _parameters)
    {

        createdMosaics = (List <String>) _parameters;
        this.setStatusCode(200);
        this.responseMessage = "Mosaics created successfully";
    }

}