﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for returning all users uploads and mosaics i HTTP format at given time.
/// </summary>
public class Timestamp
{
    /// <summary>
    /// Time of mosaicing session
    /// </summary>
    public String timestamp = "";

    /// <summary>
    /// The list of uploads thumbnails URIs
    /// </summary>
    public List<String> thumbnails = new List<String>();

    /// <summary>
    /// The list of images URIs
    /// </summary>
    public List<String> images = new List<String>();
    /// <summary>
    /// The list of mosaics URIs
    /// </summary>
    public List<String> mosaics = new List<String>();

    /// <summary>
    ///  The list of mosaics thumbnails URIs
    /// </summary>
    public List<String> mosaicsThumbnails = new List<String>();

    
}

/// <summary>
/// This class is responsible for uploaded images response.
/// </summary>
public class viewUploadedImagesResponse : Response
{
    /// <summary>
    /// The list of timestamps of users mosaicing sessions
    /// </summary>
    public List<Timestamp> timestamps = new List<Timestamp>();

    /// <param name="_parameters">parameters of the response</param>
    public override void build(Object _parameters)
    {
        timestamps = (List<Timestamp>)_parameters;
        this.setStatusCode(200);
       
    }

}