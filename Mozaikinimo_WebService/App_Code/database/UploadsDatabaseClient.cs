﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for uplouding and saving images in database.
/// </summary>
public static class UploadsDatabaseClient
    {


    /// <summary>
    /// This method is saving uploaded images to database.
    /// There are two types of images: normal one and thumbnail.
    /// </summary>
    /// <param name="savePaths">list of uploaded images save paths on server</param>
    /// <param name="userId">Authenticated user Id</param>
    public static void saveEyesUploadsInDatabase(List<String> savePaths, int userId, String timestamp)
    {
        foreach (String imageStoragePath in savePaths)
        {
            insertUploadIntoDatabase(imageStoragePath, userId, timestamp, false);
        }
    }

    /// <summary>
    /// This method saves generated mosaic in database.
    /// </summary>
    /// <param name="savePath">list of uploaded images save paths on server</param>
    /// <param name="userId">Authenticated user Id</param>
    /// <param name="timestamp">time when user started  images uploading</param>
    public static void saveMosaicInDatabase(String savePath, int userId, String timestamp)
    {
        insertUploadIntoDatabase(savePath, userId, timestamp, true);
    }

    /// <summary>
    /// This method inserts upload image to database.
    /// </summary>
    /// <param name="imageStoragePath">storage path of the image</param>
    /// <param name="userId">Authenticated user Id</param>
    /// <param name="timestamp">the current time of the event</param>
    /// <param name="moasaic">Boolean, which indicates whether upload is moaic upload or not</param>
    private static void insertUploadIntoDatabase(String imageStoragePath, int userId, String timestamp, Boolean moasaic) {


        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString);


        SqlCommand insert = new SqlCommand("insert into [uploads] (name, storagePath, userId, thumbnail, timestamp, mosaic) values(@name, @storagePath, @userId, @thumbnail, @timestamp,@mosaic)", conn);
        SqlCommand insertThumb = new SqlCommand("insert into [uploads] (name, storagePath, userId, thumbnail, timestamp, mosaic) values(@name, @storagePath, @userId, @thumbnail, @timestamp, @mosaic)", conn);

        String imageAbsolutePath = getAbsolutePath(imageStoragePath);

        try
        {
            string filename = System.IO.Path.GetFileName(imageAbsolutePath);
            string thumbStoragePath = imageStoragePath.Split('.').ElementAt(0) + "-thumb." + imageStoragePath.Split('.').ElementAt(1);
            string thumbnailName = filename.Split('.').ElementAt(0) + "-thumb." + filename.Split('.').ElementAt(1);
            string thumbName = System.IO.Path.GetFileName(getAbsolutePath(thumbStoragePath));

            insert.Parameters.AddWithValue("@name", filename);
            insert.Parameters.AddWithValue("@storagePath", imageStoragePath);
            insert.Parameters.AddWithValue("@userId", userId);
            insert.Parameters.AddWithValue("@thumbnail", 0);
            insert.Parameters.AddWithValue("@timestamp", timestamp);
            insert.Parameters.AddWithValue("@mosaic", moasaic ? 1 : 0);
            insertThumb.Parameters.AddWithValue("@name", thumbnailName);
            insertThumb.Parameters.AddWithValue("@storagePath", thumbStoragePath);
            insertThumb.Parameters.AddWithValue("@userId", userId);
            insertThumb.Parameters.AddWithValue("@thumbnail", 1);
            insertThumb.Parameters.AddWithValue("@timestamp", timestamp);
            insertThumb.Parameters.AddWithValue("@mosaic", moasaic ? 1 : 0);


            Image img = Image.FromFile(imageAbsolutePath);
            Image thumb = img.GetThumbnailImage(192, 144, null, IntPtr.Zero);
            img.Dispose();
            thumb.Save(getAbsolutePath(thumbStoragePath));


            conn.Open();
            insert.ExecuteNonQuery();
            insertThumb.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            Log.Write(ex.Message + ex.StackTrace);
        }
        finally
        {
            conn.Close();
        }


    }

    /// <summary>
    /// This method gets the absolute path of the file.
    /// </summary>
    /// <param name="relPath">relative path  of the file</param>
    /// <returns>absolute path from relative one</returns>
    private static string getAbsolutePath(string relPath)
    {

        return HttpContext.Current.Server.MapPath(relPath);
    }
}
