﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for image extension validation.
/// </summary>
public class ImageExtensionValidator : AbstractImageValidator
{
    /// <summary>
    /// Constructor of ImageExtensionValidator()
    /// </summary>
    /// <param name="_urlString">url string from file</param>
    /// <param name="_lineNumber">Line number of url in images file</param>
    public ImageExtensionValidator(String _urlString, int _lineNumber): base ( _urlString,  _lineNumber)
    { }

    /// <summary>
    /// This method checks if image extension is valid.
    /// </summary>
    public override void validate()
    {

        try
        {
            String ext = System.IO.Path.GetExtension(urlString);

            if (ext == null)
            {

                valid = false;
                errorMessage = "Image url's file contains invalid url  on line " + lineNumber;

            }
            else if (ext.ToLower() == ".jpeg" || ext.ToLower() == ".jpg" || ext.ToLower() == ".png")
            {

                valid = true;
            }
            else
            {

                valid = false;
                errorMessage = "Image url's file contains invalid url extension (" + ext + ") on line " + lineNumber + ". Possible extensions are: jpeg, png, jpg.";
            }

        }

        catch (Exception ex) {
            valid = false;
            errorMessage = "Image url's file contains invalid url extension  on line " + lineNumber + ". Possible extensions are: jpeg, png, jpg.";  
        }

    }
}