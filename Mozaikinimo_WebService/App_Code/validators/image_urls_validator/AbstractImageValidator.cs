﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This is an abstract class fro for all validators asociated with images storage paths file.
/// </summary>
public abstract class AbstractImageValidator : Validator
{
    /// <summary>
    ///  url of image path from file
    /// </summary>
    protected String urlString;
    /// <summary>
    /// Is this url valid
    /// </summary>
    protected bool valid = false;
    /// <summary>
    /// Error if url isinvalid
    /// </summary>
    protected String errorMessage = "";
    /// <summary>
    /// Line number in images file on which error occured
    /// </summary>
    protected int lineNumber;

    /// <summary>
    /// Constructor of AbstractImageValidator()
    /// </summary>
    /// <param name="_urlString">url string from file</param>
    /// <param name="_lineNumber">Line number of url in images file</param>
    protected AbstractImageValidator(String _urlString, int _lineNumber)
	{
        lineNumber = _lineNumber;
        urlString = _urlString;	
	}

    /// <summary>
    /// Initialisation of validate()
    /// </summary>
    public abstract void validate();

    /// <summary>
    /// This method returs true if url string at certain imagesfile line is valid
    /// </summary>
    /// <returns>Returns whether validation resource is valid</returns>
    public bool isValid()
    {
        return valid;
    }

    /// <summary>
    /// This method returns error message if any
    /// </summary>
    /// <returns>returns error message</returns>
    public string getErrorMessage()
    {
        return errorMessage;
    }
}