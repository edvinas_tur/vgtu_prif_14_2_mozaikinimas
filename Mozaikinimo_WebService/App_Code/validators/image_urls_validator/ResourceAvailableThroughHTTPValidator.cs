﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// This class is responsible for availability of resources through Http.
/// </summary>
public class ResourceAvailableThroughHTTPValidator : AbstractImageValidator
{
    /// <summary>
    /// Constructor of ResourceAvailableThroughHTTPValidator()
    /// </summary>
    /// <param name="_urlString">url string from file</param>
    /// <param name="_lineNumber">Line number of url in images file</param>
    public ResourceAvailableThroughHTTPValidator(String _urlString, int _lineNumber): base(_urlString, _lineNumber)
    { }

    /// <summary>
    /// This method checks if resources are available through Http.
    /// </summary>
    public override void validate()
    {
      
        HttpWebResponse response = null;

        try
        {            
            var request = (HttpWebRequest)WebRequest.Create(urlString);
            request.Method = "HEAD";
            response = (HttpWebResponse)request.GetResponse();
            valid = true;
        }
        catch (Exception ex)
        {

            if (System.IO.File.Exists(urlString))
            {
                valid = true;

            }
            else {
                valid = false;
                errorMessage = "Image with url: " + urlString + " cannot be reached over the HTTP, check your url and try again, image urls file line: " + lineNumber;
            }
          
        }
        finally
        {
            if (response != null)
            {
                response.Close();
            }
        }
    }
}