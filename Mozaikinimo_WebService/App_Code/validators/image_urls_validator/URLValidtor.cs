﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for validation of url.
/// </summary>
public class URLValidtor : AbstractImageValidator
{
    /// <summary>
    /// Constructor of URLValidtor()
    /// </summary>
    /// <param name="_urlString">url string from file</param>
    /// <param name="_lineNumber">Line number of url in images file</param>
    public URLValidtor(String _urlString, int _lineNumber) : base(_urlString, _lineNumber) { }

    /// <summary>
    /// This method checks if images URI is appropriate.
    /// </summary>
    public override void validate()
    {
        try
        {
             valid = this.IsValidUri(urlString);
      
            if (!valid) errorMessage = "Errors with your image url's file content. String (" + urlString + ") on line " + lineNumber + " is not a valid URL string";
        }
        catch (Exception ex) {
            valid = false;
            errorMessage = "Errors with your image urls' file content. String  (" + urlString + ") on line " + lineNumber + " is not a valid URL string"; 
        }
    }

    /// <summary>
    /// This method determines if String is valid URI string.
    /// </summary>
    /// <param name="uri">url variable</param>
    /// <returns>true/false. True if valid</returns>
    private Boolean IsValidUri(String uri)
    {
        try
        {
            new Uri(uri);
            return true;
        }
        catch
        {
            return false;
        }
    }
}