﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for validation of creating new users.
/// </summary>
public class CreateRegValidation : Validator
{

    private String email;
    private bool valid = true;
    private String errorMessage = "";
    private String fname;
    private String password;

    /// <summary>
    /// Constructor of CreateRegValidation()
    /// </summary>
    /// <param name="_email">an email provided by user</param>
    /// <param name="_fname">a name provided by user</param>
    /// <param name="_password">a password provided by user</param>
    public CreateRegValidation(String _email, String _fname, String _password)
    {
        email = _email;
        fname = _fname;
        password = _password;
    }

    /// <summary>
    /// This method returns error message if any.
    /// </summary>
    /// <returns>error message if any</returns>
    public string getErrorMessage()
    {
        return errorMessage;
    }

    /// <summary>
    /// This method returns valid value.
    /// </summary>
    /// <returns>true if registration is valid</returns>
    public bool isValid()
    {
        return valid;
    }

    /// <summary>
    /// This method checks if there are any disruptions in user registration and stores all errors in errors variable
    /// of validation.
    /// </summary>
    public void validate()
    {
        email_validator util = new email_validator();
        bool containsInt = password.Any(char.IsDigit);
        if (email == null || email == "")
        {

            valid = false;
            errorMessage += "email attribute in SOAP request is empty or null.  ";

        }

        if (fname == null || fname == "")
        {
            valid = false;
            errorMessage += "first name attribute in SOAP request is empty or null.  ";
        }

        if (password == null || password == "")
        {
            valid = false;
            errorMessage += "password attribute in SOAP request is empty or null.  ";
        }

        if (password.Length < 4 && password.Length>12)
        {
            valid = false;
            errorMessage += "password length should contain 5-12 symbols.   ";
        }
        if (fname.Length < 3 && fname.Length > 12)
        {
           valid = false;
            errorMessage += "first name should contain 3-12 symbols.   ";
        }
        if(containsInt == false)
        {
            valid = false;
            errorMessage += "password should contain atleast 1 digit.   ";
        }
        if (util.IsValidEmail(email) == false)
        {
            valid = false;
            errorMessage += "wrong email address.  ";
        }
    

    }
}