﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for validation of authentication.
/// </summary>
public class Authentication : Validator
{
    string password;
    string email;

    bool isDataValid;

    string dbId, dbEmail, dbFname, dbPassword, dbTimeStamp;

    string errorMessage;

    //string response;
    
    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="_password">a password provided by user</param>
    /// <param name="_email">an email provided by user </param>
    public Authentication(string _password, string _email)
    {
        password = _password;
        email = _email;

        isDataValid = true;
    }

    /// <summary>
    /// This method checks if there are any authentication disruption.
    /// </summary>
    public void validate()
    {
        if (email == null || email == "")
        {
            isDataValid = false;
            errorMessage = "Email attribute in SOAP request is empty or null.";
        }

        if (password == null || password == "")
        {
            isDataValid = false;
            errorMessage = "Password attribute in SOAP request is empty or null.";
        }

        if (isDataValid)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString))
            {
                connection.Open();

                string sqlCommand = "SELECT * FROM [users] WHERE email = @email";

                using (var command = new SqlCommand(sqlCommand, connection))
                {
                    command.Parameters.Add("@email", SqlDbType.NVarChar).Value = email;

                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        dataReader.Read();

                        try
                        {
                            dbId = dataReader["Id"].ToString();
                            dbEmail = dataReader["email"].ToString();
                            dbFname = dataReader["fname"].ToString();
                            dbPassword = dataReader["password"].ToString();
                            dbTimeStamp = dataReader["att"].ToString();
                        }
                        catch (Exception e)
                        {
                            isDataValid = false;
                            Log.Write(e.StackTrace);
                            errorMessage = "Email does not match any account.";
                            return;
                        }
                        finally {
                            connection.Close();
                        }                 
                    }
                }
              
            }

            string passwordToHash = password + dbTimeStamp;
            string hashedPassword = HashAndSalt.Hash(passwordToHash);
            
            if (hashedPassword == dbPassword)
            {
                isDataValid = true;
            }
            else
            {
                isDataValid = false;
                errorMessage = "Password is invalid, try again";
            }
        }
    }

    /// <summary>
    /// This method returns tru if authentication is valid.
    /// </summary>
    /// <returns>isDataValid</returns>
    public bool isValid()
    {
        return isDataValid;
    }

    /// <summary>
    /// This method returns error message if any.
    /// </summary>
    /// <returns>error message if any</returns>
    public string getErrorMessage()
    {
        return errorMessage;
    }

    /// <summary>
    /// This method return authenticated users Id
    /// </summary>
    /// <returns>Authenticated users ID</returns>
    public int  getUserId()
    {
        int resp = -1; 
        try
        {
            resp = Int32.Parse(dbId);
        }
        catch (Exception ex) {

        }
       
        return resp;
    }
}