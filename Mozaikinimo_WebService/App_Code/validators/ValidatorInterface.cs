﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Interface for all validators in app
/// </summary>
public interface Validator
{
    /// <summary>
    /// Method to validate resource
    /// </summary>
     void validate();
    /// <summary>
    /// Method used to determine if validated resource is valid
    /// </summary>
    /// <returns>True if resource is valid otherwise false</returns>
     bool isValid();
    /// <summary>
    /// Method whic returns all found errors in given resource validation
    /// </summary>
    /// <returns>Error message if any</returns>
     String getErrorMessage();

}