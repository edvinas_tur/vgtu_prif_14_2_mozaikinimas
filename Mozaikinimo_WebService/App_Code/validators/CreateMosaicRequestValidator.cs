﻿using System;
using System.Net;
using System.Linq;
/// <summary>
/// This class is responsible for Mosaic request validation.
/// </summary>
public class CreateMosaicRequestValidator : Validator
{

    private String file;
    private bool valid = false;
    private String errorMessage = "";
    private String algorythm;

    /// <summary>
    /// Constructor of CreateMosaicRequestValidator()
    /// </summary>
    /// <param name="_algorythm">selected algorythm string</param>
    /// <param name="_file">Images file URI</param>
    public CreateMosaicRequestValidator(String _algorythm, String _file)
	{

        file = _file;
        algorythm =_algorythm;

	}

    /// <summary>
    /// This method checks if there are any disruptions in Mosaic request
    /// </summary>
    public void validate()
    {
        valid = true;
        String ext = System.IO.Path.GetExtension(file);

        if (!(algorythm == null || algorythm == "")){
            string[] validAlgorythms = { "fast", "surf","sift" };

            if (!validAlgorythms.Contains(algorythm.ToLower())) {
                valid = false;
                errorMessage += "alogrythm attribute must be empty or set to value from this list: 'FAST, SIFT, SURF'";

            }

        
        }

        if (file == null)
        {
            valid = false;
            errorMessage += "file attribute in SOAP request is empty or null.  ";
        }
        else if (!doFileExists(file))
        {
            valid = false;
            errorMessage += "File cannot be reached over the HTTP or downloaded localy, check your url and try again. ";
        }


        if (ext == null) {
            valid = false;
            errorMessage += "docName attribute in SOAP request is empty or null.  ";
        }
        else if (ext.ToLower() != ".txt")
        {
            valid = false;
            errorMessage += "Imeges urls file's extension must be .txt  ";
        }
             
    }

    /// <summary>
    /// This method returns true if request is valid.
    /// </summary>
    /// <returns>True if mosaic request is valid</returns>
    public bool isValid()
    {
        return valid;
    }

    /// <summary>
    /// This method returns error message if any.
    /// </summary>
    /// <returns>error message if any</returns>
    public string getErrorMessage()
    {
        return errorMessage;
    }

    /// <summary>
    /// This method checks if file in given URL exists.
    /// </summary>
    /// <param name="urlString">url string variable</param>
    /// <returns>True if file in given url exists</returns>
    private bool doFileExists( String urlString)
    {
        Boolean valid = false;
        HttpWebResponse response = null;
        try
        {
            var request = (HttpWebRequest)WebRequest.Create(urlString);
            request.Method = "HEAD";
            response = (HttpWebResponse)request.GetResponse();
            valid = true;
        }
        catch (Exception ex)
        {
            if (System.IO.File.Exists(urlString))     valid = true;
            else   valid = false;
        }
        finally
        {
            if (response != null) response.Close();
        }

        return valid;
    }
}