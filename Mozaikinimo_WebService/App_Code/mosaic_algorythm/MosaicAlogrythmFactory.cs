﻿using Emgu.CV.Features2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Factory class which returns correct type of algorythm chosen by user in users request
/// </summary>
public static class MosaicAlgorythmFactory{
    /// <summary>
    /// method returns proper algorythm from provided name
    /// </summary>
    /// <param name="algorythm">Algorythm provided by user</param>
    /// <returns>Actual algorythm object of valid type</returns>
    public  static Feature2DBase<float> getAlgorythm(String algorythm)
    {

        // TODO consider switch
        if (algorythm == null)
        {
            return null;
        }
        if (algorythm.ToLower() == "sift")
        {
            return new SIFTDetector();

        }
        if (algorythm.ToLower() == "surf")
        {
            return new SURFDetector(250,false);

        }
        if (algorythm.ToLower() == "fast")
        {
            return new SURFDetector(250, false);

        }

        return null;
    }
}