﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for the validation, execution and response building  of registration request
/// </summary>
public class RegistrationRequest : BaseRequest
{

    private String email, name, password;

    /// <summary>
    /// Constructor of RegistrationRequest().
    /// </summary>
    /// <param name="email">an email provided by user</param>
    /// <param name="name">a name provided by user</param>
    /// <param name="password">a password provided by user</param>
    public RegistrationRequest(String email, String name, String password)
    {
        this.email = email;
        this.name = name;
        this.password = password;
    }

    /// <summary>
    /// This method creates new user (saves in database) and does its validation.
    /// </summary>
    public override void execute()
    {

        if (email == null || name == null || password == null)
        {
            isRequestValid = false;
            response = new BadRequestResponse();
            response.build("email, name and password attributes are required");
            return;
        }


        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString);
        con.Open();
        Validator regValidator = new CreateRegValidation(email, name, password);
        regValidator.validate();
        try
        {
            if (regValidator.isValid())
            {
                isRequestValid = true;
                DateTime myDate = new DateTime();
                //string hashedPassword = HashAndSalt.GenerateKeyHash(password);
                String timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                string finallyHashed = password + timeStamp;
                string something = HashAndSalt.Hash(finallyHashed);
                SqlCommand cmd = new SqlCommand("insert into [users] values(@fname, @email, @password, @att)", con);
                cmd.Parameters.AddWithValue("@fname", email);
                cmd.Parameters.AddWithValue("@email", name);
                cmd.Parameters.AddWithValue("@password", something);
                cmd.Parameters.AddWithValue("@att", timeStamp);
                cmd.ExecuteNonQuery();

                response = new SuccessfullRegistrationResponse();
                response.build("Data stored succesfully");
            }
            else
            {
                isRequestValid = false;
                response = new BadRequestResponse();
                response.build(regValidator.getErrorMessage());
            }
        }
        catch (Exception ex)
        {
            isRequestValid = false;
            response = new BadRequestResponse();
            response.build("This email already exists");


        }

        con.Close();
        return;
    }
}
