﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// An abstract class for the handling all the requests. Each request will have its special response and each request can be valid or not
/// </summary>
public abstract class BaseRequest
{
    protected Response response = null;
    protected Boolean isRequestValid = false;

    /// <summary>
    /// This method returns current requessts response which type can change dynamicly during valiation process.
    /// </summary>
    /// <returns>Response to concrete request in given time moment</returns>
    public Response getResponse()
    {
        return this.response;
    }
    /// <summary>
    /// Abstract method which executes current request. All sub classes must implement this method
    /// </summary>
    public abstract void execute();

    /// <summary>
    /// This method checks if the request is valid.
    /// </summary>
    /// <returns>tru if request is valid or false if not</returns>
    public Boolean isValid()
    {
        return isRequestValid;
    }


}
