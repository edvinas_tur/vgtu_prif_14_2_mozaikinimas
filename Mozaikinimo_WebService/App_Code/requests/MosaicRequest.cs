﻿using Emgu.CV.Features2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// This class is responsible for the requests of mosaic generation.
/// </summary>
public class MosaicRequest : BaseRequest
{

    int userId = -1;
    String algorythm = null;
    String file = null;

    /// <summary>
    /// Constructor of MosaicRequest().
    /// </summary>
    /// <param name="userId">Current authenticated user ID</param>
    /// <param name="algorythm">selected algorythm string</param>
    /// <param name="file">storage path or url of file in which  all images storage paths are stored</param>
    public MosaicRequest(int userId, String algorythm, String file)
    {
        this.userId = userId;
        this.algorythm = algorythm;
        this.file = file;
    }

    /// <summary>
    /// Method responsible for executing mosaic request.
    /// </summary>
    public override void execute()
    {

        isRequestValid = true;
        Validator requestValidator = new CreateMosaicRequestValidator(algorythm, file);
        // If request params are invalid
        requestValidator.validate();

        if (!requestValidator.isValid())
        {
            response = new BadRequestResponse();
            response.build(requestValidator.getErrorMessage());
            isRequestValid = false;
            return;
        }


        Feature2DBase<float> alg = MosaicAlgorythmFactory.getAlgorythm(algorythm);

        if (alg != null)
        {
            response = new BadRequestResponse();
            response.build("SIFT, SURF AND FAST based mosaic algorythms are not supported yet.");
            return;
        }

        // Doing work from here...
        ImagesUploader imagesUploader = new ImagesUploader();

        if (imagesUploader.uploadImagesFromFile(file, userId))
        {
            EyeImageGrouper grouper = new EyeImageGrouper();

            if (!grouper.groupImages(imagesUploader.getImagesStoragePaths(), imagesUploader.getStoragePath(), userId, imagesUploader.getTimestamp())) {
                response = new BadRequestResponse();
                response.build("Cannot group images, please try again later.");
                isRequestValid = false;
                return;
            }

            List<String> leftEyes = grouper.getLeftEyeImgages();
            List<String> rightEyes = grouper.getRightEyeImgages();

            String errors = null;
            String leftEyesavePath  =  createMosaic(leftEyes,"Left", imagesUploader.getTimestamp(), ref errors);
            String rightEyesavePath = createMosaic(rightEyes, "Right", imagesUploader.getTimestamp(), ref errors);

            if (errors != null) {
                response = new BadRequestResponse();
                response.build("Message from mosaic generator: "+ errors);
                isRequestValid = false;
                return;

            }

            List<String> mosaicsPaths = new List<String>();
            if (leftEyesavePath != null) mosaicsPaths.Add(leftEyesavePath);
            if (rightEyesavePath != null) mosaicsPaths.Add(rightEyesavePath);

            response = new MosaicResponse();
            response.build(mosaicsPaths);

        }
        else
        {
            isRequestValid = false;
            response = new BadImageUrlsFileResponse();
            response.build(imagesUploader.getValidators());
            return;
        }
    }

    /// <summary>
    /// This method checks if there are any errors in creating mosaic.
    /// </summary>
    /// <param name="images">uplouded images list of storage paths</param>
    /// <param name="mode">mode of mosaic generator. can be Left for left eyes or Right for right eyes</param>
    /// <param name="timestamp">Time when user invoked mosaic request</param>
    /// <param name="errors">Reference to errros object to populate it if any</param>
    /// <returns></returns>
    private String createMosaic(List<String> images, String mode, String timestamp , ref String errors) {

        MosaicGenerator generator = new MosaicGenerator(images, mode);
        String savePath = generator.createMosaicUsingStitcherCPU(System.Web.Hosting.HostingEnvironment.MapPath("~\\Storage\\"), userId, timestamp);
      

        if (generator.getErrors() != null) {
            if (errors == null)
            {
                errors = generator.getErrors();
            }
            else {
                errors += ". " +generator.getErrors();
            }        
        }
        if (savePath == null) return null;

        return buildMosaicResponseHTTPUri(savePath);
    }

    /// <summary>
    /// This method builds the response of mosaic in HTTP URI format which can be opened in browser or etc.
    /// </summary>
    /// <param name="stroragePath">path of the storage</param>
    /// <returns>HTTP uri of resource</returns>
    private String  buildMosaicResponseHTTPUri(String stroragePath) {
   
        String output = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Authority, stroragePath);
        output = output.Replace("~", "");
        output = output.Replace("\\", "/");
        return output;

    }   
}
