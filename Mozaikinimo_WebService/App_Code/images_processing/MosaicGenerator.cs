﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.Util;
using Emgu.CV.GPU;
using System.Web;
using Emgu.CV.Stitching;

/// <summary>
/// This class is responsible for the generation of mosaic.
/// </summary>
public class MosaicGenerator
{
    private List<String> images = null;
    private string type = "";
    private string errors = null;

    /// <summary>
    /// Constructor of MosaicGenerator().
    /// </summary>
    /// <param name="_images">images strage paths from which mosaic is beeing generated</param>
    /// <param name="type">Type of mosaic. Can be f.e. left or right mosaic</param>
    public MosaicGenerator(List<String> _images, String type)
    {
        if (_images != null)
        {

            this.images = new List<String>();
            this.images = _images;
            this.type = type;
        }
    }

    /// <summary>
    /// This method creates mosaic using stitcher CPU and saves it.
    /// </summary>
    /// <param name="storage">storage path folder on server in which mosaics will be saved</param>
    /// <param name="userId">an unique user ID</param>
    /// <param name="timestamp">timestamp of time when user invoked mosaic upload method</param>
    /// <returns>Path in which generated mosaic is stored</returns>
    public String createMosaicUsingStitcherCPU(String storage, int userId, String timestamp)
    {
        String pathForDatabaseSave = null;
        if (images != null)
        {

            try
           {
                Image<Bgr, Byte>[] loadedImages = new Image<Bgr, Byte>[images.Count];

                int i = 0;
                foreach (String path in images)
                {

                    loadedImages[i] = new Image<Bgr, Byte>(getHttpUriFromPath(path));
                    i++;


                }
         
                using (Stitcher stitcher = new Stitcher(
                  false
                  ))
                {

                    
                    Image<Bgr, Byte> result = stitcher.Stitch(loadedImages);
         
                    String mosaicImageName = type + "_eye_mosaic_" + timestamp + ".JPG";
                    String saveRlativePath = System.Web.Hosting.HostingEnvironment.MapPath("~\\Storage\\" + userId + "\\" + timestamp + "\\") + mosaicImageName;// +userId +"\\"+ timestamp + "\\" + mosaicImageName;
                    pathForDatabaseSave = "~\\Storage\\" + userId + "\\" + timestamp + "\\" + mosaicImageName;
                    result.Save(saveRlativePath);

                    for (int z = 0; z < loadedImages.Length; z++)
                    {
                        loadedImages[z].Dispose();
                    }

                    loadedImages = null;

                    if (result != null)
                    {
                        UploadsDatabaseClient.saveMosaicInDatabase(pathForDatabaseSave, userId, timestamp);
                    }
                }
            }
            catch (Exception ex)
            {
                errors = ex.Message;
                Log.Write(ex.Message + ex.StackTrace);
            }
        }

        return pathForDatabaseSave;
    }

    /// <summary>
    /// This method returns errors if there are any.
    /// </summary>
    /// <returns>List of errors asociated with generating of the mosaic</returns>
    public String getErrors() {

        return this.errors;
    }

    /// <summary>
    /// This method creates mosaic using other algorythms like SURF, FAST, SIFT.
    /// </summary>
    /// <param name="algorythm">used algorythm</param>
    /// <param name="storage">storage path</param>
    /// <param name="userId">Authenticated user Id</param>
    /// <param name="timestamp">the cuurent time of the event</param>
    public void createMsoaicUsingAlgorythm(Feature2DBase<float> algorythm, String storage, int userId, String timestamp)
    {

        // TODO implement image stlitching sufing SURF, FAST AND SIFT
        /*

        */
    }

    /// <summary>
    /// This method finds matches in images using homography matrix from descriptors. Method not finished, because of problems.
    /// </summary>
    /// <param name="algorythm">selected algorythm</param>
    /// <param name="modelIamge">modeled image storage path</param>
    /// <param name="observedImage">observed image storage path</param>
    /// <returns>processed image</returns>
    private Image<Bgr, Byte> FindMatch(Feature2DBase<float> algorythm, Image<Bgr, Byte> modelIamge, Image<Bgr, Byte> observedImage)
    {
        HomographyMatrix homography = null;
        Feature2DBase<float> detectorCPU = algorythm;


        int k = 2;
        double uniquenessThreshold = 0.8;
        Matrix<int> indices;

        Matrix<byte> mask;

        VectorOfKeyPoint modelKeyPoints;
        VectorOfKeyPoint observedKeyPoints;


        Image<Gray, Byte> fImageG = modelIamge.Convert<Gray, Byte>();
        Image<Gray, Byte> lImageG = observedImage.Convert<Gray, Byte>();


        //extract features from the object image
        modelKeyPoints = new VectorOfKeyPoint();
        Matrix<float> modelDescriptors = detectorCPU.DetectAndCompute(fImageG, null, modelKeyPoints);


        // extract features from the observed image
        observedKeyPoints = new VectorOfKeyPoint();
        Matrix<float> observedDescriptors = detectorCPU.DetectAndCompute(lImageG, null, observedKeyPoints);
        BruteForceMatcher<float> matcher = new BruteForceMatcher<float>(DistanceType.L2);
        matcher.Add(modelDescriptors);

        indices = new Matrix<int>(observedDescriptors.Rows, k);
        using (Matrix<float> dist = new Matrix<float>(observedDescriptors.Rows, k))
        {
            matcher.KnnMatch(observedDescriptors, indices, dist, k, null);
            mask = new Matrix<byte>(dist.Rows, 1);
            mask.SetValue(255);
            Features2DToolbox.VoteForUniqueness(dist, uniquenessThreshold, mask);
        }

        int nonZeroCount = CvInvoke.cvCountNonZero(mask);
        if (nonZeroCount >= 4)
        {
            nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, indices, mask, 1.5, 20);
            if (nonZeroCount >= 4)
                homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints, observedKeyPoints, indices, mask, 2);
        }

        modelKeyPoints.Dispose();
        observedKeyPoints.Dispose();
        fImageG.Dispose();
        lImageG.Dispose();

        Image<Bgr, Byte> mImage = modelIamge.Convert<Bgr, Byte>();
        Image<Bgr, Byte> oImage = observedImage.Convert<Bgr, Byte>();


        Image<Bgr, Byte> result = new Image<Bgr, byte>(mImage.Width + oImage.Width, mImage.Height);

        if (homography != null)
        {

            Rectangle rect = new Rectangle(0, 0, 2000, 2000);
            PointF[] pts = new PointF[] {
                 new PointF(rect.Left, rect.Bottom),
                 new PointF(rect.Right, rect.Bottom),
                 new PointF(rect.Right, rect.Top),
                 new PointF(rect.Left, rect.Top)};
            homography.ProjectPoints(pts);

            result = oImage.WarpPerspective(homography, homography.Width, homography.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR, Emgu.CV.CvEnum.WARP.CV_WARP_INVERSE_MAP, new Bgr(200, 0, 0));
            mImage.Dispose();
            oImage.Dispose();
            homography.Dispose();
            return result;
        }
        return null;
    }

    /// <summary>
    /// This method gets a path in HTTP.
    /// </summary>
    /// <param name="stroragePath">path of the storage</param>
    /// <returns>HTTP uri from relative storage path</returns>
    private string getHttpUriFromPath(String stroragePath)
    {

        return stroragePath == null ? null : HttpContext.Current.Server.MapPath(stroragePath);
    }


}


