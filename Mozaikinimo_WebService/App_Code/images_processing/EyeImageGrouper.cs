﻿using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Web.Hosting;
using System.Web.UI;


/// <summary>
/// This class is responsible for the grouping of images.
/// Images are grouping to right and left eyes.
/// Images are grouped by userIds folders and timestamp folders.
/// </summary>
public class EyeImageGrouper
{

    /// <summary>
    /// The list of leftEyes
    /// </summary>
    private List<String> leftEyes = null;

    /// <summary>
    /// The list of rightEyes
    /// </summary>
    private List<String> rightEyes = null;

    /// <summary>
    /// Timestamp which indicates when uer staretd images upload
    /// </summary>
    private String timestamp = null;

    /// <summary>
    /// Initialisation of cotrol to work with servers relative path's.
    /// </summary>
    private Control control = new Control();

    /// <summary>
    /// This method is grouping images to left and right eyes and stores results in separate folders on server. True is returned if operation was successfull.
    /// </summary>
    /// <param name="images">uplouded images</param>
    /// <param name="storagePath">storage path</param>
    /// <param name="userId">Authenticated user Id</param>
    /// <param name="_timestamp">the current time of the event</param>
    /// <returns>true if everything processed successfully or false if not</returns>
    /// 


    public bool groupImages(List<String> images, String storagePath, int userId, String _timestamp)
    {

        timestamp = _timestamp;
        leftEyes = new List<String>();
        rightEyes = new List<String>();

        const int increment = 10; // 10 px kvadrateliais ziurime paveiksleli
        Image<Bgr, Byte> img = null;
        double[,] RGBIntensityAreas = null;


        try
        {
            foreach (String imageStoragePath in images)
            {

                img = new Image<Bgr, Byte>(getAbsolutePath(imageStoragePath)); // loading new image using EmguCV   
                RGBIntensityAreas = new double[img.Height + increment, img.Width + increment]; // pridejus increment mes galim buti tikri, jog tikrai neiseisime uz masyvo ribu
                                                                                               // Šiame cikle yra formuojama RGB sumų matrica kiekvienam pikseliui paveikslėlyje sudetingumas: O(m + increment)*(n + increment))
                for (int i = 0; i < img.Height; i += increment)
                    for (int j = 0; j < img.Width; j += increment)
                        for (int z = i; z < (i + increment) && (i + increment) < img.Height; z++)
                            for (int y = j; y < (j + increment) && (j + increment) < img.Width; y++)
                                RGBIntensityAreas[i, j] += img.Data[z, y, 0] + img.Data[z, y, 1] + img.Data[z, y, 2];
                //  

                int XCoordinate = getXCoordinatesOfPixel(RGBIntensityAreas, RGBIntensityAreas.Cast<double>().Max()); // Gauname koordinates to pixeliu kvadratelio, kuriame yra didžiausią RGB instensyvumo suma

                if (XCoordinate < img.Width / 2) // reiškia kairė akis nes į kairę nuo centro 
                    leftEyes.Add(moveImageFileToGroup(storagePath, imageStoragePath, "Left"));
                else //reiškia į dešinę ir tai dešinė akis
                    rightEyes.Add(moveImageFileToGroup(storagePath, imageStoragePath, "Right"));

                img.Dispose();
            }

            saveEyeImageGroups(leftEyes, rightEyes, userId);
        }
        catch (Exception ex)
        {

            Log.Write(ex.Message + ex.StackTrace);
            return false;

        }

        return true;
    }

    /// <summary>
    /// This method is moving grouped images to specified  group folder: left or right eye folder.
    /// </summary>
    /// <param name="storage">storage</param>
    /// <param name="imageStoragePath">the path of the image</param>
    /// <param name="group">specified group can be either: Left or Right</param>
    /// <returns>Path in which image saved / moved to</returns>
    private String moveImageFileToGroup(String storage, String imageStoragePath, String group)
    {


        String savePath = storage + group + "Eyes\\" + System.IO.Path.GetFileName(getAbsolutePath(imageStoragePath));
        System.IO.Directory.CreateDirectory(getAbsolutePath(storage) + group + "Eyes\\");
        System.IO.File.Move(getAbsolutePath(imageStoragePath), getAbsolutePath(savePath));

        return savePath;
    }

    /// <summary>
    /// This method returns the list of leftEyes.
    /// </summary>
    /// <returns>String list of left Eye images</returns>
    public List<String> getLeftEyeImgages()
    {

        return leftEyes;
    }

    /// <summary>
    /// This method returns the list of rightEyes.
    /// </summary>
    /// <returns> String list of right Eye images</returns>
    public List<String> getRightEyeImgages()
    {
        return rightEyes;
    }

    /// <summary>
    /// This method gets coordinates from pixels.
    /// </summary>
    /// <typeparam name="T">variable T</typeparam>
    /// <param name="matrix">matrix used for computing</param>
    /// <param name="value">the value of T</param>
    /// <returns>y coordinate of pixel in image RGB matrix</returns>
    private int getXCoordinatesOfPixel<T>(double[,] matrix, T value)
    {
        for (int x = 0; x < matrix.GetLength(0); ++x)
            for (int y = 0; y < matrix.GetLength(1); ++y)
                if (matrix[x, y].Equals(value))
                    return y; // y bus lygi eilutei (t.y X) nes vaizdas yra išdėstomas stulpeliais
        return -1;
    }



    /// <summary>
    /// This method saves the groups of eye images to database.
    /// </summary>
    /// <param name="leftEyes">images of the left eye</param>
    /// <param name="rightEyes">images of the right eye</param>
    /// <param name="userId">and unique user ID</param>
    private void saveEyeImageGroups(List<String> leftEyes, List<String> rightEyes, int userId)
    {
        UploadsDatabaseClient.saveEyesUploadsInDatabase(leftEyes, userId, timestamp);
        UploadsDatabaseClient.saveEyesUploadsInDatabase(rightEyes, userId, timestamp);
    }

    /// <summary>
    /// This method gets the paths of files.
    /// </summary>
    /// <param name="relPath">the path of the image</param>
    /// <returns>absolute Path from relative</returns>
    private string getAbsolutePath(string relPath)
    {

        return HttpContext.Current.Server.MapPath(relPath);
    }
}