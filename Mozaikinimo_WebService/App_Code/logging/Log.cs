﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Static class to log all WS exceptions to log file in storage.
/// </summary>
public static class Log
{

    /// <summary>
    /// This method logs exceptions to log file.
    /// </summary>
    /// <param name="logMessage">the message to log</param>
    public static void Write(string logMessage)
    {


        try
        {
            using (StreamWriter w = File.AppendText(System.Web.Hosting.HostingEnvironment.MapPath("~\\Storage\\") + "log.txt"))
            {
                try
                {
                    w.Write("\r\nLog Entry : ");
                    w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                        DateTime.Now.ToLongDateString());
                    w.WriteLine("  :");
                    w.WriteLine("  :{0}", logMessage);
                    w.WriteLine("-------------------------------");
                }
                catch (Exception ex)
                {
                   
                }
            }
        }
        catch (Exception ex)
        {
            
        }
    }
}





